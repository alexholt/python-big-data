import pandas as pd
import numpy as np 
from sklearn.cluster import KMeans 
from matplotlib import pyplot as plt 


# Plot the clusters with the data points and cluster centroids
def plot_clusters(k, data, kmeans):
    labels = kmeans.labels_
    centroids = kmeans.cluster_centers_
    for i in range(k):
        # select only data observations with cluster label == i
        ds = data[np.where(labels==i)]
        # plot the data observations
        plt.plot(ds[:,0], ds[:,1], 'o')
        # plot the centroids
        lines = plt.plot(centroids[i,0], centroids[i,1], 'kx')
        # make the centroid x's bigger
        plt.setp(lines, ms=15.0)
        plt.setp(lines, mew=2.0)
    plt.show()

    
# Read csv-file from disk into dataframe object
df = pd.read_csv('data/data_1024.csv', sep='\t')
print(df.shape)
print(df.head())

# Get data as ndarray (2-dimensional)
data = df[['Distance_Feature','Speeding_Feature']].values

# Plot the data before it's analyzed
plt.plot(data[:,0], data[:,1], 'o')
plt.xlabel('Distance Feature')
plt.ylabel('Speeding Feature')
plt.show()

# Create models with different k
wcss = []
for k in range (1, 7):
    kmeans = KMeans(n_clusters=k).fit(data)
    plot_clusters(k, data, kmeans)
    wcss.append(kmeans.inertia_)

# Plot the elbow analysis to find optimal k
plt.plot(range(1, 7), wcss)
plt.title('The elbow method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS') #within cluster sum of squares
plt.show()

#
# https://github.com/datascienceinc/learn-data-science/blob/master/Introduction-to-K-means-Clustering/Notebooks/Introduction-to-K-means-Clustering-Example.ipynb
#

# Try some simple predictions
kmeans = KMeans(n_clusters=4).fit(data)
print(kmeans.predict([[50, 5], [200, 80], [50, 5], [150, 10], [50, 40]]))

