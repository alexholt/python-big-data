#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: sander

"""

import pandas as pd
import numpy as np
import math

from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from sklearn.metrics import confusion_matrix


# Function for choosing k
def choose_k(dataframe):
	k = int(math.sqrt(dataframe.shape[0]))
	if k % 2 == 0:
		k = k + 1
	
	return k


# Load data and preliminary exploration
df = pd.read_csv('data/diabetes.csv')
# print(df.head())
# print(df.shape)
# print(df.dtypes)

#Wash
zero_not_accepted = ['Glucose', 'BloodPressure', 'SkinThickness', 'BMI']
for column_name in zero_not_accepted:
	df[column_name] = df[column_name].replace(0, np.nan)
	mean = df[column_name].mean(skipna=True)
	df[column_name] = df[column_name].replace(np.nan, mean)

# Split training/test
y = df['Outcome'] 
X = df.drop('Outcome', axis=1)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

# Normalize
scaler = StandardScaler()
scaler.fit(X_train)
X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)

# Choose k (see top for function)
k = choose_k(y_test)

# Train
knn = KNeighborsClassifier(n_neighbors=k, metric='euclidean')
knn.fit(X_train_scaled, y_train)

# Predict
predicted_values = knn.predict(X_test_scaled)

# Assess
cm = confusion_matrix(y_test, predicted_values)
print(cm)
